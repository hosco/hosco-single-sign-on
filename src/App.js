import React from 'react';
import { useLocation } from 'react-router-dom';
import jwt from 'jsonwebtoken';
import jwkToPem from'jwk-to-pem';
import './App.css';

function App() {
  // aws key
  const jwk = {};
  const pem = jwkToPem(jwk);

  const location = useLocation();
  const id_token = getURLHashParameter("id_token", location);

  if(id_token) {
    jwt.verify(id_token, pem, { algorithms: ['RS256'] }, function(err, decodedToken) {
    console.log(decodedToken);
    console.log(err);
  });

  }
  

  return (
   
      <div className="hosco-sign-in-btn">
        <a className="hosco-sign-in-link" href=""> 
          <div className="hosco-sign-in-container">
              <div className="hosco-sign-in-logo">
                <img alt="hosco logo" src="https://cdn.hosco.com/hosco/h-hosco.png" width="22px" heigh="22px"/>
              </div>
              <div className="hosco-sign-in-text-container">
                <span className="hosco-sign-in-text"> 
                  Sign in with Hosco 
                </span>
              </div>
          </div>
        </a>
      </div>
  
  );
}

const getURLHashParameter = function(name, location) {
        return decodeURI(
            (RegExp('[#|&]' + name + '=' + '(.+?)(&|$)').exec(location.hash)||[,null])[1]
        );
    };


export default App;
