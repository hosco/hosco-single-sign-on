## This is an example of how you can decode and retrieve the AWS Cognito token that comes from the Hosco Hosted UI sign-in

In this case it is a React Repository. To test this:

1. Inform us so we can redirect your SSO Hosted UI callback to http://localhost:3000 
2. Run npm install
3. Add the aws key given to you by Hosco as the `jwk` const in App.js
4. Add in the href field of the SSO button the SSO modal address provided by Hosco
5. Run npm start
6. Click on Sign-in with Hosco
7. Sign-in using a User's Hosco credentials
8. You will see the decoded token in the browser's console.
9. Once you test this, we please let us know, and we will redirect the SSO form back to your login URL.